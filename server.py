import zmq
import random
import sys
import time
from application import Application as apps

port = "50000"
context = zmq.Context()
socket = context.socket(zmq.REP)
socket.bind("tcp://*:%s" % port)

while True:
    #  Wait for next request from client
    message = socket.recv_string()
    print("Received request: ", message)
    time.sleep(1)
    message = apps.run_commandline()
    socket.send_string(message)