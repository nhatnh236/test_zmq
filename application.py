import subprocess

class Application:

    @staticmethod
    def run_commandline():
        arrCmd = ['echo', 'hello client']
        logs = subprocess.run(arrCmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        if logs.returncode == 0:
            out = (logs.stdout).decode()
        else:
            out = (logs.stderr).decode()
        return out

